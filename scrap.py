# https://findrulesoforigin.org/api/countries TODOS LOS PAISES
# https://findrulesoforigin.org/api/getftalist?showall=false TODOS LOS ACUERDOS
# https://findrulesoforigin.org/api/getftareportingctrystatus?ftaid=645 TODOS LOS PAISES DE LOS ACUERDOS

# https://findrulesoforigin.org/api/getftaroo?reporter=032&partner=152&product=03011110 LA REGLA POR PRODUCTO (34011110)


# https://findrulesoforigin.org/api/getftatotalprovisions?ftaid=645 TODOS LOS TEXTOS POR LINEA
# https://findrulesoforigin.org/api/products-by-keyword?countryCode=032&level=100&keyword=8703 BUSQUEDA PRODUCTOS

 # Module sys has to be imported:
import sys                
import pandas as pd
import requests
import ast
import json
import urllib.request
from pandas.io.json import json_normalize



# Iteration over all arguments:
# print(sys.argv[0])
# print(sys.argv[1])
# print(sys.argv[2])
# for eachArg in sys.argv:   
#         print(eachArg)


def  getftacountries(ftaid):
    url = "https://findrulesoforigin.org/api/getftareportingctrystatus?ftaid=" + str(ftaid)
    r = requests.get(url, headers={'Accept': 'application/json'})
    data = pd.DataFrame(r.json())
    return data


def  getftaroo(reporter,partner,code):
    url = "https://findrulesoforigin.org/api/getftaroo?reporter=" + reporter + "&partner=" + partner + "&product=" + code + ""
    df = pd.read_json(url)
    data = json_normalize(df['FtaRooData'])
    return data

def  getftalist():
    url = "https://findrulesoforigin.org/api/getftalist?showall=false"
    r = requests.get(url, headers={'Accept': 'application/json'})
    data = pd.DataFrame(r.json())
    # ['FtaId', 'FtaName', 'FtaStatus', 'FtaYear', 'IsSigned', 'InNegotiation','FtaShortName']
    return data.sort_values('FtaId')

def  getcountries():
    url = "https://findrulesoforigin.org/api/countries"
    r = requests.get(url, headers={'Accept': 'application/json'})
    data = pd.DataFrame(r.json())
    return data

def  getftatext(ftaid):
    url = "https://findrulesoforigin.org/api/getftatotalprovisions?ftaid=" + str(ftaid)
    r = requests.get(url, headers={'Accept': 'application/json'})
    data = pd.DataFrame(r.json())
    return data
    # return data.sort_values('ArticleTitleRank','ArticleDescRank')


############ VARS #################
cous = [
    "arg","bhs" ,"brb","blz","bol","bra","chl","col","cri","dom","ecu","slv","gtm","guy","hti","hnd","jam","mex","nic","pan","pry","per","sur","tto","uru","ven"
]
############ END VARS #############

# print(getcountries().columns)
# print(cous[1])
# print(getcountries())





########### CREATE COUNTRIES LIST  ####################
# c_df = getcountries()
# cous_filtered = c_df[c_df.ISO3.str.lower().isin(cous)]
# cou_idb = cous_filtered.Code.values.tolist()   # IDB26 reporter countries 
# cou_all = c_df.Code.values.tolist()
# print(cou_idb)
# print(cou_all)
################# END ################################# 


########### CREATE FTA ID LIST  ####################
fta_ls = getftalist().FtaId.values.tolist()
print(fta_ls)
################# END ################################# 







# country_mapper = {k:v for (k,v) in zip(c_df["ISO3"], c_df["Code"])}


# df["country_id"] = df["CO_PAIS"].astype(int).map(country_mapper).str.lower()
# print(country_mapper)
# c_df["id"] = 
# print(getftaroo())
# print(getftalist().columns)

# reporter = '032'
# partner = '152'
# code = '03011110'
# df = getftaroo(reporter,partner,code)

# export_csv = df.to_csv ('./export_dataframe.csv', index = None, header=True)
# export_excel = df.to_excel ('./export_dataframe_03011110.xlsx', index = None, header=True) 
# export_excel = df.to_excel ('./export_dataframe_34011110.xlsx', index = None, header=True) 



##########################################    TO CSV   ########################################################################

######################### CREATE FTA CSV ##############
# df = getftalist()
# df.to_csv('0_fta_list.csv', encoding='utf-8', index=False)
######################### END ######################### 

######################### CREATE COUNTRIES CSV ########
# df = getcountries()
# df.to_csv('1_countries_list.csv', encoding='utf-8', index=False)
######################### END ######################### 

######################### CREATE COUNTRIES with FTA ID CSV ########
# df = pd.DataFrame()
# for id in fta_ls:
#     df1 = getftacountries(id)
#     df1["fta_id"] = id
#     df2 = df.append(df1, ignore_index=True, sort=False)
#     print(id)
#     # print(df2.head())
#     with open('2_fta_countries.csv', 'a') as f:
#         df2.to_csv(f, mode='a', header=f.tell()==0, encoding='utf-8', index=False)
######################### END ######################### 


######################### CREATE FTA TEXT CSV ########
# print(df.ArticleTitleRank.isnull().sum())
#df.to_csv('3_fta_text.csv', encoding='utf-8', index=False)
df = pd.DataFrame()
for id in fta_ls:
    df1 = getftatext(id)
    print(id)
    with open('3_fta_text.csv', 'a') as f:
        df1.to_csv(f, mode='a', header=f.tell()==0, encoding='utf-8', index=False)
######################### END ######################### 
